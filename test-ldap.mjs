import ldap from 'ldapjs'; 

const ldapUrl = 'ldap://127.0.0.1:10389';
const humanAmy = 'cn=Amy Wong,ou=people,dc=planetexpress,dc=com';
const robotRodrig = 'cn=Bender Bending Rodríguez,ou=people,dc=planetexpress,dc=com';


async function findGenre(utilisateur) {
  return new Promise(resolve => {

      var genre = undefined;
      
      var opts = {
        filter: '(objectclass=inetOrgPerson)',
        scope: 'sub',
        attributes: ['description']};

        var client = ldap.createClient({
          url: [ldapUrl]
        });

        
      client.search(utilisateur, opts, function(err, res) {

        res.on('searchEntry', function(entry) {
          console.log('entry: ' + JSON.stringify(entry.object));
          genre = entry.object.description;
          // genre = entry.description; 
        });
        res.on('searchReference', function(referral) {
          console.log('referral: ' + referral.uris.join());
        });
        res.on('error', function(err) {
          console.error('error: ' + err.message);
        });
        res.on('end', function(result) {
          // console.log('status: ' + result.status);
          client.unbind(function(err) {

          resolve(genre);

          });
        });
      });
      
  });
}

const genreAmy = await findGenre(humanAmy);
const genreRodrig = await findGenre(robotRodrig);

console.log('Amy is ' + genreAmy);
console.log('Rodrig is ' + genreRodrig);