const { createClient } = require('ldapjs');

const ldapUrl = 'ldap://127.0.0.1:10389';

async function findGenre(utilisateur) {
  return new Promise(resolve => {

      var genre = undefined;
      
      var opts = {
        filter: '(objectclass=inetOrgPerson)',
        scope: 'sub',
        attributes: ['description']};

        var client = createClient({
          url: [ldapUrl]
        });

        
      client.search(utilisateur, opts, function(err, res) {

        res.on('searchEntry', function(entry) {
          console.log('entry: ' + JSON.stringify(entry.object));

          genre = entry.object.description;

        });
        res.on('searchReference', function(referral) {
          console.log('referral: ' + referral.uris.join());
        });
        res.on('error', function(err) {
          console.error('error: ' + err.message);
        });
        res.on('end', function(result) {
          console.log('status: ' + result.status);
          client.unbind(function(err) {

          //TODO retourner un objet genre
          resolve(genre);

          });
        });
      });
      
  });
}

module.exports = {
  findGenre
}