'use strict'

console.log("main js findGenre");
const { findGenre } = require ('./ldap-genre');
console.log(findGenre);

async function isHuman (user) {
    const genre = await findGenre('cn='+ user +','+ 'ou=people,dc=planetexpress,dc=com');
    console.log('genre from ldap => ' + genre)
    let result=false;
    if (genre == 'Human'){  
        result = true
    }
    return result;
}

module.exports = {
    isHuman
}