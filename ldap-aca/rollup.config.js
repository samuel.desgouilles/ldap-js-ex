import commonjs from '@rollup/plugin-commonjs';
import nodeResolve from '@rollup/plugin-node-resolve';
import builtins from 'rollup-plugin-node-builtins';
import strip from '@rollup/plugin-strip';

export default {
  input: 'src/index.js',
  output:
  [
    { file: 'dist/ldap-aca-cjs.js', format: 'cjs', exports: 'named' }
    // ,{ file: 'dist/ldap-aca-esm.js', format: 'esm',exports: 'named' },
    // ,{ file: 'dist/ldap-aca-def.js' }
    //,{ file: 'dist/ldap-aca-umd.js', format: 'umd' , name:'ldap-acaU' }
  ],
  
  plugins: [
    commonjs(),// Convert CommonJS modules to ES6
    nodeResolve({preferBuiltins: false}),//Locate and bundle third-party dependencies in node_modules
    // strip({functions: [ 'console.log'],})// supprimer console.log et autre assert.
    builtins()
]
};
