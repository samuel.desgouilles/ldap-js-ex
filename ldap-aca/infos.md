pour la lib
    npm init

    npm install ldapjs
    sudo dnf install make gcc
    npm install dtrace-provider --save

=> 
    genere les fichier package* 
    telecharge les modules dans le repertoire node_modules

pour la construction du module: 

npm install --global rollup

npm install @rollup/plugin-node-resolve --save-dev
    =>les dependances npm font parties du bundle
npm install @rollup/plugin-commonjs --save-dev
    => au format pour nodejs
npm install @rollup/plugin-json --save-dev
    => pas obligatoire ( test pour lescture des import requiere depuis le fichier package.json ??)

npm run build

=====
https://www.jvandemo.com/a-10-minute-primer-to-javascript-modules-module-formats-module-loaders-and-module-bundlers/

Different Module Formats

As JavaScript originally had no concept of modules, a variety of competing formats have emerged over time. Here’s a list of the main ones to be aware of:

    The Asynchronous Module Definition (AMD) format is used in browsers and uses a define function to define modules.
    The CommonJS (CJS) format is used in Node.js and uses require and module.exports to define dependencies and modules. The npm ecosystem is built upon this format.
    The ES Module (ESM) format. As of ES6 (ES2015), JavaScript supports a native module format. It uses an export keyword to export a module’s public API and an import keyword to import it.
    The System.register format was designed to support ES6 modules within ES5.
    The Universal Module Definition (UMD) format can be used both in the browser and in Node.js. It’s useful when a module needs to be imported by a number of different module loaders.

=====
https://rollupjs.org/guide/en/#es-module-syntax
=====
//commonJS

    //commonJS default export , but does not specify the name to be used by consumers of the module
    module.exports = isHuman;
    // => const nimp = require()

    // named export
    exports.estHumain = isHuman;
    //=> const nimp = require("").estHumain;
    //    const MyList = require("./list").LinkedList;

// // javascript module 
//     //default export
//     export default class LinkedList {}
//     // import nimp from ""

//     // named
//     export class LinkedList {}
//     //=>import { LinkedList } from "./linked-list.js";
       //import { LinkedList as MyList } from "./list.js";


// module.exports = {
//     isHuman,
//     default: isHuman,
//   }



===

utilisation de strip pour virer console.log  erreur : jse_cause: Error [InvalidAsn1Error]: Expected 0x4: got 0x30

peut limiter le scope de ce qui est stripper par defaut: strip({functions: [ 'console.log'],})

===
SyntaxError: Cannot use import statement outside a module
https://nodejs.org/api/packages.html#packages_determining_module_system



import builtins from 'rollup-plugin-node-builtins';
import globals from 'rollup-plugin-node-globals'